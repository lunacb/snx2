module Snx2
  class Build
    def initialize
      @list = []
    end

    def add(*artifacts)
      @list += artifacts.flatten
    end

    def exec(**args)
      @list.each { |a| a.build(**args) }
    end
  end
end
