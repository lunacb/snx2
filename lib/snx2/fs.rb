module Snx2
  module FS
    class TreeNode
      attr_accessor :path, :generic_path, :parent, :type, :children

      def initialize(path, generic_path=nil, parent=nil)
        path = Snx2::Path.new path
        os_path = path.to_os
        generic_path = Snx2::Path.new(generic_path)

        @path = path
        @generic_path = generic_path
        @parent = parent

        begin
          @type = :dir
          @children = Dir.each_child(os_path).map do |a|
            [
              a,
              TreeNode.new(path + a, generic_path + a, self)
            ]
          end.to_h

        rescue Errno::ENOTDIR
          @type = (parent != nil)?(:file):(:none)

        rescue Errno::ENOENT
          @type = :none
        end
      end

      def node_at(path)
        path = Snx2::Path.new path

        pos = self
        node_none = TreeNode.new(@path + path, @generic_path + path, nil)

        path.components.filter { |a| ["","."].none? a }.each do |a|
          return none_node if pos.type != :dir
          pos = pos.children[a]
          return node_none if pos == nil
        end
        pos
      end

      def crawl
        enum = Enumerator.new do |y|
          case @type
          when :dir
            @children.each_value { |a| a.crawl { |b| y << b } }
          when :file
            y << self
          end
        end

        if block_given? then enum.each { |a| yield a }
        else                 return enum
        end
      end

      # TODO: Make this more efficient. Empty directories are still left behind
      # and can still be crawled through later.
      def take
        if @parent != nil
          @parent.children.delete_if { |_,v| v == self }
        end
        @type = :none
      end
    end

    class NodeSet
      attr_reader :set

      def initialize(set)
        # Array of `Item`s
        @set = set
      end

      def filter
        @set.filter! { |a| yield a.payload }
        return self
      end

      def peek
        @set.map { |a| a.payload }
      end

      def take
        @set.each { |a| a.node.take }
        peek
      end

      class Item
        attr_accessor :node, :payload
        def initialize(node, payload)
          @node = node
          @payload = payload
        end
      end
    end

    class TreeFile
      attr_reader :src, :outputs

      def initialize(node, **args)
        env = Snx2::Env.new(nil, **args)
        @src = node.path.to_os
        @outputs = Snx2::OutputList.new(env.outputs, node.generic_path)
      end

      def link
        return { @src => @outputs },
          # TODO: map paths to outputs, not arrays of pathname components to outputs.
          @outputs.map { |k,v| [v.generic.components.clone, @outputs] }.to_h
      end
    end

    class Tree
      def initialize(path, generic_path=nil)
        @path = Snx2::Path.new path
        @root = TreeNode.new(@path, generic_path)
      end

      def subtree(path, generic_path=nil)
        # TODO
      end

      def gather_files(path, **args)
        NodeSet.new(@root.node_at(path).crawl.map do |a|
          NodeSet::Item.new(a, TreeFile.new(a, **args))
        end)
      end

      def gather_docs(path, **args)
        NodeSet.new(@root.node_at(path).crawl.filter_map do |a|
          env = Snx2::Env.new(nil, **args)
          src = a.path.to_os
          outputs = Snx2::OutputList.new(env.outputs, a.generic_path, type: :doc)

          # TODO: Make the ".snx" a constant somewhere.
          NodeSet::Item.new(a, Snx2::Doc.new(src, outputs)) if a.path.has_suffix? ".snx"
        end)
      end

      def peek_template(generic, specific=nil, **args)
        generic = @path + generic
        specific = specific.transform_values { |a| @path + a } if specific != nil
        return Snx2::Template.new(generic, specific, **args)
      end

      def take_template(generic, specific=nil, **args)
        generic = @path + generic
        specific = specific.transform_values { |a| @path + a } if specific != nil
        res = Snx2::Template.new(generic, specific, **args)
        res.each_value { |a| @root.node_at(a.path.remove_prefix(@path)).take }
        return res
      end
    end
  end
end
