require "snx2/util/partial_class"

module Snx2
  class Vars
    include Snx2::Util::PartialHash

    attr_reader :hash

    def initialize(from=nil, **args)
      @hash = (from != nil)?(from.to_h.merge args):({})
    end

    # TODO: Better name than "output name"
    #       Maybe "output group"? And my OutputList can be an OutputGroups?
    #       or maybe somoething more generic than an "output *" because we're
    #       using it as a key for other things too.
    def generate_specific(output, emitter, links)
      @hash.transform_values { |a| generate_var a, output, emitter, links }
    end

    private

    def generate_var(v, output, emitter, links)
      if v.respond_to? :generate_specific
        v.generate_specific output, emitter, links
      elsif v.is_a? Array
        v.map { |a| generate_var a, output, emitter, links }
      elsif v.is_a? Hash
        v.transform_values { |a| generate_var a, output, emitter, links }
      else
        v
      end
    end
  end
end
