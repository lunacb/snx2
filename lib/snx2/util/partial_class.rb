require "set"

module Snx2
  # TODO: Add most of the features of the underlying classes
  # TODO: Use a better naming scheme for underlying classes
  # TODO: Add sanitation/verification
  module Util
    module PartialHash
      include Enumerable

      def each(&block); @hash.each(&block); end
      def each_key(&block); @hash.each_key(&block); end
      def each_value(&block); @hash.each_value(&block); end
      def delete(key); @hash.delete(key); end

      def [](key); @hash[key]; end
      def []=(key, value); @hash[key] = value; end

      def merge!(*list); @hash.merge!(*list.map(&:to_h)); self; end

      def merge(*list)
        res = self.class.new self
        res.merge!(*list)
      end

      def to_h; @hash; end
    end

    module PartialArray
      include Enumerable

      def each(&block); @array.each(&block); end

      def [](i); @array[i]; end
      def []=(i, value); @array[i] = value; end

      def length; @array.length; end

      # TODO: Should this be a clone?
      def to_a; @array; end
    end

    module PartialSet
      include Enumerable

      def each(&block); @set.each(&block); end

      def merge(other); @set.merge sanitize_enum(other); self; end
      def union(other); clone.tap { |a| a.merge sanitize_enum(other) }; end
      # TODO: Should this be here since it's not in Set?
      def push(*others); @set.merge sanitize_enum(others); self; end
      alias | union
      alias + union

      def add(o); @set.add sanitize_item(o); self; end

      def ==(other); @set == Set.new(other); end

      def to_a; @set.to_a; end

      private

      # When an attempt is made to add an item to the set somehow, this method
      # is called on the item and the return value is used as the actual item.
      # You can override this to change the behavior.
      def sanitize_item(item); item; end

      def sanitize_enum(enum);
        raise ArgumentError, "Value must be Enumerable" unless enum.is_a? Enumerable
        enum.map { |a| sanitize_item a };
      end
    end
  end
end
