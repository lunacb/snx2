require "erb"
require "snx2/util/partial_class"

module Snx2
  class Template
    include Snx2::Util::PartialHash

    SUFFIX = ".snxt"

    class Item
      attr_reader :body, :path
      def initialize(body, path)
        @body, @path = body, path
      end
    end

    attr_reader :hash

    def load_for(key)
      @hash[key] ||= first_file([
        @specific_map[key],
        @generic_path.change_suffix(nil, ".#{key.to_s}#{SUFFIX}"),
      ], true) || @generic_item
    end

    def initialize(generic, specific=nil, **args)
      specific ||= {}
      env = Snx2::Env.new(nil, **args)

      @generic_path = Snx2::Path.new generic
      @specific_map = specific
      @generic_item = first_file([
        @generic_path,
        @generic_path.change_suffix(nil, SUFFIX),
      ], false)

      @hash = {}
      env.dests.each { |a| load_for a }
    end

    private

    # Takes an array of `Snx2::Path`s, also accepting `nil`s that are ignored.
    # Finds the first one that refers to a real file. and returns that file, or
    # nil if none of the files exist. If `required` is true, then stops at the
    # first `Snx2::Path` in the array.
    def first_file(paths, required)
      paths.each do |a|
        next if a == nil

        begin
          File.open(a.to_os, "r") do |b|
            return Snx2::Template::Item.new(ERB.new(b.read), a)
          end
        rescue Errno::ENOENT
          return nil if required
        end
      end
      return nil
    end
  end
end
