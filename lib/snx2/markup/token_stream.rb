# TODO: Write unit tests

module Snx2
  module Markup
    module Patterns
      # Match constants that will be used later in the program have a named "main"
      # capture group that describes the portion of the matched text that is actually
      # special. Everything outside of it should be considered regular text.
      VAR = /[a-zA-Z0-9-]+/

      ONELINE_WS = /[ \t]/

      # Do a lookbehind search to ensure that the following character isn't
      # backslash-escaped.
      UNESCAPED = /(?<!\\)(?:\\{2})*/

      # More forgiving than the builtin word boundary anchor.
      BOUNDARY = /(?=\s)|\Z/

      HEADING = /
          (?<main>
            # The vertical bar at the beginning.
            ^\|#{ONELINE_WS}*
            (?:
              (?<var_match>)
              # A variable name.
              (?<name>#{VAR}):
              # Everything after it.
              (?<text>.*)
            )?$
          )
        /x

      DIRECTIVE = /
          #{UNESCAPED}
          (?<main>
            # The directive itself.
            \[
              (?<directive> [^\]\R]* )
            #{UNESCAPED} \]
            # Optional whitespace after the directive.
            #{ONELINE_WS}*
            # Capture the implicit or explicit directive body start.
            (?<directive_start> [:{] )
          )
        /x

      DIRECTIVE_BODY_END = /
          #{UNESCAPED}
          (?<main>
            (?<directive_end> } )
          )
        /x

      ALIAS_START = /^#{ONELINE_WS}*+(?!\\)/

      HEADING_ALIAS = /
          (?<main>
            #{ALIAS_START}
            (?<heading_alias>
              # Some number of pound symbols.
              \#+
              #{BOUNDARY}
            )
          )
        /x

      LISTITEM_ALIAS = /
          (?<main>
            #{ALIAS_START}
            (?<listitem_alias>
              \*
              #{BOUNDARY}
            )
          )
        /x

      BODY = /
          (?:(?<directive_match>)#{DIRECTIVE})
          |(?:(?<directive_body_end_match>)#{DIRECTIVE_BODY_END})
          |(?:(?<heading_alias_match>)#{HEADING_ALIAS})
          |(?:(?<listitem_alias_match>)#{LISTITEM_ALIAS})
        /x
    end

    class TokenStream
      def initialize(stream)
        @lines = stream.readlines
      end

      def each_token()
        enum = Enumerator.new do |y|
          body = @lines.each.drop_while do |line|
            if line =~ Patterns::HEADING
              if $~[:var_match] != nil
                y << token(:var, key: $~[:name].downcase, value: $~[:text].strip)
              end
              true
            end
          end.join

          # Used by directive alias regular expressions.
          body = "\n" + body

          until body.empty?
            if body =~ Patterns::BODY
              match = $~
              pre, post = match.string[0...match.begin(:main)], match.string[match.end(:main)...]
              y << token(:text, contents: unescaped_str(pre)) unless pre.empty?
              body = post

              if match[:directive_match] != nil
                args = unescaped_split(match[:directive])

                parsed = case args[0]
                         when "break",       "b"   then [ token(:break), { } ]
                         when "heading",     "#"   then [ token(:heading), { level: :number } ]
                         when "paragraph",   "p"   then [ token(:paragraph), { } ]
                         when "quote",       "q"   then [ token(:quote), { } ]
                         when "code",        "c"   then [ token(:code), { } ]
                         when "list",        "l"   then [ token(:list), { } ]
                         when "ordered",     "o"   then [ token(:ordered), { } ]
                         when "item",        "*"   then [ token(:item), { } ]
                         when "link",        "=>"  then [ token(:link), { pattern: :link } ]
                         when "embed",       ">>"  then [ token(:embed), { pattern: :link } ]
                         when "^normal",     "^n"  then [ token(:normal_text), { } ]
                         when "^bold",       "^b"  then [ token(:bold_text), { } ]
                         when "^italic",     "^i"  then [ token(:italic_text), { } ]
                         when "^bolditalic", "^bi" then [ token(:bolditalic_text), { } ]
                         when "^code",       "^c"  then [ token(:code_text), { } ]
                         else nil
                         end

                directive = if parsed == nil
                  if args[0] == nil
                    token(:error, text: "Empty directive")
                  else
                    token(:error, text: "Unknown directive \"#{args[0]}\"")
                  end
                else
                  parse_directive(args[1..], *parsed)
                end

                y << token(:directive,
                  explicit: (match[:directive_start] == "{"),
                  directive: directive
                )
              elsif match[:directive_body_end_match] != nil
                y << token(:directive_end)
              elsif match[:heading_alias_match] != nil
                y << token(:directive,
                  explicit: false,
                  directive: token(:heading, level: match[:heading_alias].length)
                )
              elsif match[:listitem_alias_match] != nil
                y << token(:directive, explicit: false, directive: token(:item))
              end
            else
              y << token(:text, contents: unescaped_str(body))
              break
            end
          end
        end


        if block_given? then enum.each { |a| yield a }
        else                 enum
        end
      end

      private

      def token(type, **data)
        return { type: type }.merge(data)
      end

      def unescaped_str(str)
        return str.gsub(/\\./) { |a| a[1] }
      end

      def unescaped_split(str)
        # TODO: Fix this.
        str.split(/(?<!\\)(?:\\{2})*\s+/).filter_map { |a| unescaped_str a unless a.empty? }
      end

      def parse_directive(args, fill, params)
        if args.length != params.length
          return token(:error, text: "Expected #{params.length} arguments, got #{args.length}")
        end

        params.zip(args).each do |(param,arg)|
          case param[1]
          when :number
            return token(:error, text: "Invalid argument \"#{arg}\"") if arg !~ /^[0-9]*$/
            fill[param[0]] = arg.to_i
          when :link
            # Treat this as a regular string instead of a link for now.
            # TODO: Make it better.
            fill[param[0]] = arg
          else
            raise "Invalid argument type of #{param[1]}"
          end
        end
        return fill
      end
    end
  end
end
