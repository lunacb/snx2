require "set"
require "snx2/util/partial_class"

module Snx2
  # The Env class maintains an environment that can be used to pass repetitive
  # arguments to methods in a less verbose way. When a method wants to use an
  # env to recieve arguments, you'll see the argument referenced as
  # +Env.VARIABLE+ in the documentation, where +VARIABLE+ is one of:
  # Env.dests,
  # Env.emitters,
  # Env.links,
  # Env.outputs,
  # Env.vars.
  #
  # When a method uses environment arguments, all of those arguments will be
  # cloned before used or stored, so you're safe to modify an environment after
  # a method uses it without fear of your modifications affecting portions of
  # the environment that were stored elsewhere by the method.
  #
  # TODO: A big example.
  #
  # == Propagation
  #
  # Environment arguments are propagated towards a method that accepts them in
  # a few ways. Later argument definitions shadow over earlier ones, for
  # example an if there were multiple of Env.outputs in different places then
  # the set of outputs in both would be visible, except in cases where they
  # conflicted, in which case only the later output would be visible. The
  # methods of propagation are, in order:
  # * The current gloablly bound Env, if not explicitly ignored, which is set
  #   inside the block of an Env#use or Env::new method call.
  # * The +from+ argument of the Env::new method, if called directly, specifying
  #   an Env instance.
  # * The +env+ keyword argument to an accepting method, which should be an Env
  #   instance.
  # * Individual environment keyword arguments. For example, the +dests+
  #   keyword argument would specify the Env#dests environment argument.
  class Env
    class Dests
        include Snx2::Util::PartialSet

        def initialize(*list)
          @set = Set[*list]
        end

        private

        def sanitize_item(item)
          begin
            item.to_sym
          rescue NoMethodError
            raise ArgumentError.new "Value must be representable as a Symbol"
          end
        end
    end

    # An Array of destination keys. Each key is a Symbol globally identifying a
    # single destination, which is likely associated with a single Emitter
    # format. They're also expected to match keys used in other classes, like
    # Outputs.
    #
    # Example:
    #   Snx2::Env.new do |env|
    #     env.dests.push :html, :gemini, :rss
    #   end
    attr_reader :dests

    # An EmitterList of destination emitters. Env.dests keys are expected to
    # correspond to keys here.
    #
    # TODO: Talk why I care about them and why they can be filled in by default
    #
    # Example:
    #   # TODO
    attr_reader :emitters

    # A Links list.
    #
    # TODO: Why does it matter?
    attr_reader :links

    # An OutputList that'll usually contain root destination output directories
    # that will be appended to by recieving methods. Env.dests keys are
    # expected to correspond to keys here.
    #
    # Example:
    #   Snx2::Env.new do |env|
    #     env.outputs[:html] = Snx2::Output.new(prefix: "./dest/html")
    #   end
    attr_reader :outputs

    # A Vars list containing varaibles that are made available to templates.
    attr_reader :vars

    @@current = nil

    # Returns the current globally set Env, or nil if none is set.
    def self.global; @@current; end

    # Initialize a new Env. Accepts all environment keyword arguments.
    # If +from+ is an Env, it'll be applied to the new environment after the
    # global. If +ignore_global+ is set to 'false' then the global will not be
    # incorporated into the new environment. All environment arguments
    # propagated to this new instance will be cloned, so later modifications to
    # those arguments won't be present in the instance, except where cloning
    # isn't deep enough to make a full copy. See the documentation for the
    # classes of the individual arguments for details.
    #
    # Can accept a block, which behaves the same way as the Env#use block.
    #
    # When a method accepts an environment, it will do so by taking a set of
    # keyword arguments and passing them to the Env initializer like this:
    #   def some_functoin(**args)
    #     env = Snx2::Env.new(nil, **args)
    #   end
    def initialize(from = nil, ignore_global = false, **args, &block)
      # Create the empty environment.
      @outputs = Snx2::OutputList.new
      @emitters = Snx2::EmitterList.new
      @vars = Snx2::Vars.new
      @links = Snx2::Links.new
      @dests = Dests.new

      # First merge all the `env` objects we have available.
      merge_env @@current unless ignore_global
      merge_env from
      merge_env args[:env]

      # Then merge the specific env keyword arguments.
      @outputs.merge!(Snx2::OutputList.new(args[:outputs])) if args[:outputs] != nil
      @emitters.merge!(Snx2::EmitterList.new(args[:emitters])) if args[:emitters] != nil
      @vars.merge!(Snx2::Vars.new(args[:vars])) if args[:vars] != nil
      @links.merge!(Snx2::Links.new(args[:links])) if args[:links] != nil
      @dests += args[:dests] if args[:dests] != nil

      use(&block) if block_given?
    end

    # Takes a block, which will be passed the Env the method was called on.
    # Inside the block, the environment will be set as the current global, so
    # it'll be propagated to other environments that are created.
    def use
      old_current = @@current
      @@current = self
      yield self
      @@current = old_current
    end

    private

    def merge_env(env)
      return if env == nil
      @outputs.merge!(env.outputs)
      @emitters.merge!(env.emitters)
      @vars.merge!(env.vars)
      @links.merge!(env.links)
      @dests += env.dests
    end
  end
end
