require "fileutils"

module Snx2
  module Emitters
    class Gemini
      SUFFIX = ".gmi"

      Emitters::Names ||= {}
      Emitters::Names[:gemini] = Gemini.new

      def os_path(output)
        return (output.prefix + output.generic_with_suffix(SUFFIX)).to_os
      end

      def present_path(output)
        return (Snx2::Path.new("/") + output.generic_with_suffix(SUFFIX)).to_s
      end

      def result(doc, output, links)
        return doc.tokens.join("\n\n")
      end

      def write(output, buf)
        path = os_path output
        FileUtils.mkdir_p File.dirname(path)
        File.open(path, "w") { |a| a.write buf }
      end
    end
  end
end
