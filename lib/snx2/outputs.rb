require "snx2/util/partial_class"

# TODO: Sanitize input and panic appropriately.
module Snx2
  class Output
    attr_reader :prefix, :generic, :type, :suffix, :rmsuffix, :universal

    def initialize(from=nil, generic_path="", **args)
      if from != nil
        @generic = from.generic + generic_path
        @type = from.type
        @rmsuffix = from.rmsuffix.clone
        @suffix = from.suffix.clone
        @prefix = from.prefix.clone
        @universal = from.universal
      else
        @generic = Snx2::Path.new generic_path
        @type = :regular
        @rmsuffix = nil
        @suffix = nil
        @universal = false
      end

      @prefix = Snx2::Path.new(args[:prefix]) if args[:prefix] != nil
      @type = args[:type] if args[:type] != nil
      @rmsuffix = args[:rmsuffix] if args[:rmsuffix] != nil
      @suffix = args[:suffix] if args[:suffix] != nil
      @universal = args[:universal] if args[:universal] != nil
    end

    # TODO: Should I get rid of this?
    def self.from(prefix, **args)
      return Snx2::Output.new(prefix: prefix, **args)
    end

    def generic_with_suffix(suffix)
      rmsuffix = @rmsuffix
      rmsuffix ||= ".snx" if @type == :doc
      suffix = @suffix || suffix
      res = Snx2::Path.new @generic
      res.change_suffix!(rmsuffix, suffix) if suffix != nil
      res
    end

    def ==(other)
      return false unless other.is_a? self.class
      return false if @generic != other.generic
      return false if @type != other.type
      return false if @rmsuffix != other.rmsuffix
      return false if @suffix != other.suffix
      return false if @prefix != other.prefix
      return false if @universal != other.universal
      true
    end
  end

  class OutputList
    include Snx2::Util::PartialHash

    attr_reader :hash

    def initialize(from = nil, generic_path = nil, **args)
      @hash = {}
      if from != nil
        from.each { |k,v| @hash[k] = Output.new(v, generic_path, **args) }
      end
    end

    def merge!(*list)
      @hash.merge!(*(list.map { |a| a.map { |k,v| [k, Snx2::Output.new(v)] }.to_h }))
      self
    end

    def ==(other)
      @hash == other.hash
    end
  end
end
