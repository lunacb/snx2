require  "snx2/util/partial_class"
require "snx2/emitters/gemini"
require "snx2/emitters/html"

module Snx2
  module Emitters
    Names ||= {}
  end

  class EmitterList
    include Snx2::Util::PartialHash

    attr_reader :hash

    def initialize(from = nil)
      @hash = {}
      from.each { |k,v| @hash[k] = v.clone } if from != nil
    end

    def [](key)
      @hash[key] || Snx2::Emitters::Names[key]
    end
  end
end
