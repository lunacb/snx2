module Snx2
  # A Path stores and does operations on a filesystem pathname.
  #
  # NOTE: Window support is untested.
  class Path
    # Base error class.
    class Error < ::StandardError; end

    # Raised if an invalid path is created. 
    class InvalidError < Error; end

    CLEAN_SEP_REGEX = /#{Regexp.escape(File::SEPARATOR)}/
    private_constant :CLEAN_SEP_REGEX
    SEPARATOR_REGEX = (File::ALT_SEPARATOR != nil) \
      ? /(?:#{Regexp.escape(File::SEPARATOR)}|#{Regexp.escape(File::ALT_SEPARATOR)})/ \
      : CLEAN_SEP_REGEX
    private_constant :SEPARATOR_REGEX

    # Create a new Path using +from+, which is converted to a string if not one
    # already using its +to_s+ method and interpreted as a pathname, with the
    # same semantics as pathnames elsewhere in Ruby.
    def initialize(from); @str = clean from.to_s; end

    # Convert the pathname to a string. The string will be cleaned as much as
    # possible without accessing the filesystem, by simplifying redundant
    # filename separators, +.+ components, and +..+ componets. The string will
    # have pathname components separated by forward slashes (+/+).
    def to_s; @str.clone; end

    # Converts +other+ into a Path and compares it to +self+, returning +true+
    # or +false+. Two paths are considered equal if their cleaned string
    # versions (#to_s) are equal.
    def eql?(other); clean(other.to_s) == @str; end
    alias == eql?
    alias === eql?

    # Used to add new pathname components onto the end of +self+. Converts each
    # argument to a Path and appends them in order onto the end of +self+.
    # Absolute paths are treated the same as relative paths: Both simply have
    # their pathname componens appended. Raises a Path::InvalidError exception
    # if the resulting path is not equal to or a sub-path of the original.
    # Returns +self+.
    #
    # Examples:
    #   Snx2::Path.new("a/b").descend!("/c/d")     #=> "a/b/c/d"
    #   Snx2::Path.new("a/b").descend!("./c/../d") #=> "a/b/d"
    #   Snx2::Path.new("a/b").descend!(".")        #=> "a/b"
    #   Snx2::Path.new("a/b").descend!("..")       #=> Snx2::Path::InvalidError
    def descend!(*others)
      other = File.join others.map(&:to_s)
      newp = clean File.join(@str, other)

      # Check if any component of the new pathname is actually contained within
      # `@str`.
      safe = case @str
      when "."
        # The only thing that would break out of "." would begin at its parent
        # directory ("..")
        newp !~ /\A\.\.(?:\Z|#{CLEAN_SEP_REGEX})/
      when File::SEPARATOR
        # You can't go outside of root. 
        true
      when /(?:\A|#{CLEAN_SEP_REGEX})\.\.\Z/
        # `@str` consists entirely of ".."'s. `newp` will always begin with the
        # same number of ".."'s because Path doesn't know how to remove them,
        # only how to add them. So the only unsafe case is where `newp` steps
        # outwards to the parent directory of `@str`, which will only be the
        # case when the pathname component after what's common between `@str`
        # and `newp` is a "..".
        newp.length == @str.length or \
          newp[(@str.length + 1)...] !~ /\A\.\.(?:\Z|#{CLEAN_SEP_REGEX})/
      else
        # `@str` ends with a regular filename. That means `newp` is safe if
        # either the two are identical or `newp` has additional pathname
        # components on the end that `@str` doesn't have. As long as `newp`
        # begins with `@str`, all the components after what's common between
        # the two will be regular (not ".."), because ".." only appears at the
        # beginning of a pathname string.
        newp.start_with? @str and \
          (newp.length == @str.length or sep_start?(newp[@str.length...]))
      end

      if not safe
        raise Path::InvalidError,
          "the added pathname (#{other.dump}) breaks out of self (#{@str.dump})"
      end

      @str = newp
      self
    end

    # Same as #descend!, but returns a copy of +self+ descended by +others+.
    def descend(*others); clone.descend!(*others); end

    alias + descend

    # Each argument is concatenated onto the end of +self+, after being
    # converted to a Path. If a joined path is encountered that is absolute,
    # all previous paths will be discarded and the new path will begin at the
    # encountered absolute path. Returns +self+.
    #
    # Examples:
    #   Snx2::Path.new("a/b").join!("/c/d")     #=> "/c/d"
    #   Snx2::Path.new("a/b").join!("./c/../d") #=> "a/b/d"
    #   Snx2::Path.new("a/b").join!(".")        #=> "a/b"
    #   Snx2::Path.new("a/b").join!("..")       #=> "a"
    def join!(*others)
      @str = clean \
        others.map(&:to_s).reduce(@str) { |a,b| (sep_start? b)?(b):(File.join(a, b)) }
      self
    end

    # Same as #join!, but returns a copy of +self+ joined with +others+.
    def join(*others); clone.join!(*others); end

    # Returns +true+ if the path has the extension +ext+, and +false+
    # otherwise. The extension is present if the basename of the path ends with
    # it, but is not counted if the extension and the basename are identical.
    # There must be characters in the basename before the extension.
    #
    # Examples:
    #   Snx2::Path.new("a/b.txt").has_ext? ".txt"  #=> true
    #   Snx2::Path.new("a/b.txt").has_ext? "b.txt" #=> false
    #   Snx2::Path.new("aaa/bbb").has_ext? "a/bbb" #=> false
    def has_ext?(ext)
      ext !~ SEPARATOR_REGEX and \
        @str.end_with? ext and \
        @str.length != ext.length and \
        @str[-1-ext.length] !~ SEPARATOR_REGEX
    end

    # Adds the extension to the pathname. Wile rase a Path::InvalidError if the
    # method is run on a Path equal to +.+ or +/+. Returns +self+.
    #
    # Examples:
    #   Snx2::Path.new("a/b").add_ext! ".txt" #=> "a/b.txt"
    #   Snx2::Path.new(".").add_ext! ".txt"   #=> Snx2::Path::InvalidError
    def add_ext!(ext)
      if ext =~ SEPARATOR_REGEX
        raise ArgumentError,
          "extension #{ext.dump} cannot contain a filepath separator"
      end

      if @str == "." or @str == File::SEPARATOR
        # TODO: Use a better exception than ArgumentError.
        raise Path::InvalidError,
          "An extension cannot be added to a \".\" or \"#{File::SEPARATOR}\" pathname."
      end
      @str += ext
      self
    end

    # Same as #add_ext!, but returns a copy of +self+ with +ext+ added.
    def add_ext(ext); clone.add_ext!(ext); end

    # Removes the extension if it exists (see #has_ext?). Returns +true+ if the
    # extension was removed and +false+ otherwise.
    def rm_ext!(ext);
      has_ext?(ext).tap do |has_ext|
        @str = @str[...-ext.length] if has_ext
      end
    end

    # Returns a copy of +self+ with the extension +ext+ removed if it exists
    # (see #has_ext?).
    #
    # Examples:
    #   Snx2::Path.new("a/b.txt").has_ext? ".txt"  #=> "a/b"
    #   Snx2::Path.new("a/b.txt").has_ext? "b.txt" #=> "a/b.txt"
    #   Snx2::Path.new("aaa/bbb").has_ext? "a/bbb" #=> "aaa/bbb"
    def rm_ext(ext); clone.tap { |a| a.rm_ext!(ext) }; end

    private

    def sep_start?(str); str.start_with? SEPARATOR_REGEX; end

    def clean(pathname)
      parts = []
      dotdot = 0
      is_root = sep_start? pathname
      pathname.split(SEPARATOR_REGEX).each do |a|
        next if a == "." or a == ""
        if a == ".."
          if parts.empty? then dotdot += 1
          else                 parts.pop
          end
        else
          parts.push a
        end
      end

      return File::SEPARATOR + File.join(parts) if is_root
      return "." if parts.empty? and dotdot == 0
      return File.join(*([".."] * dotdot), *parts)
    end
  end
end
