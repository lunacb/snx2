module Snx2
  class Links
    OUTPUT_PREFIX = "#OUTPUT"

    attr_reader :paths, :generic_paths, :prefixes
    def initialize(from=nil)
      @paths = {}
      @generic_paths = {}
      @prefixes = {}

      if from != nil
        from.paths.each { |k,v| @paths[k] = v.clone }
        from.generic_paths.each { |k,v| @generic_paths[k] = v.clone }
        from.prefixes.each { |k,v| @prefixes[k] = v.clone }
      end
    end

    # Each memeber of the flattend `items` must either be a hashmap containing
    # source OS paths as keys and outputs as values, or have a method named
    # `link` that returns a tuple of one of those hashmaps and another one
    # mapping generic output paths to outputs.
    def add_paths(*items)
      items.flatten.each do |a|
        if a.is_a? Hash
          link = a
        else
          link, generic_link = a.link
          generic_link.each do |k,v|
            @generic_paths[k] = v
          end
        end

        link.each do |k,v|
          @paths[File.realpath(k)] = v
        end
      end
    end

    def add_prefix(key, value)
      @prefixes[key.to_s] = expand(Snx2::Path.new value)
    end

    def resolve(path, rel=".")
      path, rel = expand(Snx2::Path.new path), Snx2::Path.new(rel)

      if path.components[0] == OUTPUT_PREFIX then
        return @generic_paths[path.components[1...]]
      else
        begin
          return @paths[File.realpath(path.to_os, rel.to_os)]
        rescue Errno::ENOENT
          return nil
        end
      end
    end

    def merge!(with)
      @paths.merge! with.paths
      @generic_paths.merge! with.generic_paths
      @prefixes.merge! with.prefixes
      self
    end

    def merge(with)
      res = self.class.new self
      res.merge! with
    end

    private

    def expand(path)
      if (path.components[0]||"") =~ /^#(.*)/ and (prefix=@prefixes[$1]) != nil
        path.components[0..0] = prefix.components
      end
      path
    end
  end
end
