module Snx2
  module Artifact
    class Template
      class Specific
        # TODO
      end

      attr_reader :template, :vars, :outputs

      def initialize(template, **args)
        env = Snx2::Env.new(nil, **args)
        @template = template
        @vars = Snx2::Vars.new env.vars
        @outputs = Snx2::OutputList.new env.outputs
      end

      def build(**args)
        env = Snx2::Env.new(nil, **args)

        env.dests.each do |a|
          # TODO: Panic if these don't exist.
          emitter = env.emitters[a]
          output = @outputs[a]

          vars = @vars.generate_specific(output, emitter, env.links)
          buf = @template[a].body.result_with_hash(vars)

          emitter.write(output, buf)
        end
      end
    end
  end
end
