module Snx2
  class Doc
    class Specific
      attr_reader :vars, :tags, :body
      def initialize(vars, tags, body)
        @vars, @tags, @body = vars, tags, body
      end
    end

    attr_reader :src, :outputs, :tokens, :vars, :tags

    def initialize(src, outputs)
      @src = src.clone
      @outputs = Snx2::OutputList.new outputs
      @tokens = []
      @tags = []
      @vars = {}
      # Read the token stream into the class instance.
      File.open(@src) do |a|
        Snx2::Markup::TokenStream.new(a).each_token do |b|
          if b[:type] == :var
            # Store variables in a hash, and do special processing on some of
            # them.
            case b[:key]
            when "tags"
              @tags = b[:value].split(/[,[:space:]]/).filter_map { |c| c.downcase unless c.empty? }
            end

            @vars[b[:key].to_sym] = b[:value]
          else
            @tokens.push b
          end
        end
      end
    end

    def generate_specific(output, emitter, links)
      return Specific.new(@vars, @tags, emitter.result(self, output, links))
    end

    def link
      return { @src => @outputs },
        @outputs.map { |k,v| [v.generic.components.clone, @outputs] }.to_h
    end

    def xml_escape
      # TODO
    end

    def xml_escape!
      # TODO
    end
  end

  class DocTag
    attr_accessor :tag, :docs

    def initialize(tag, docs)
      @tag = tag
      @docs = docs
    end

    def self.from_docs(*docs)
      tags = {}
      docs.flatten.each do |a|
        a.tags.each do |b|
          tags[b] ||= []
          tags[b].push a
        end
      end
      return tags.map { |k,v| self.new(k, v) }
    end
  end
end
