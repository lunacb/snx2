require "debug"

require "test/unit"

include Snx2

class EnvTest < Test::Unit::TestCase
  def test_clean
    assert Path.new("").to_s == "."
    assert Path.new("/.//./../..//..").to_s == "/"
    assert Path.new("///.//./../..//../abc").to_s == "/abc"
    assert Path.new("/./a/").to_s == "/a"
    assert Path.new("a//bc/d/").to_s == "a/bc/d"
    assert Path.new("a/././../b////").to_s == "b"
    assert Path.new("./../.././.././../a/../b/../c").to_s == "../../../../c"
    assert Path.new("a/b/ccc/dd/e/../../c/../z/../../../f").to_s == "a/f"
    assert Path.new("a/../").to_s == "."
  end

  def test_eql
    assert Path.new("") == Path.new("./a/..")
    assert Path.new("a") == Path.new("a")
    assert Path.new("a") != Path.new("b")
    assert Path.new("/a") != Path.new("./a")
  end

  def test_descend
    assert Path.new("a") + "b" == "a/b"
    assert Path.new("/") + "a" == "/a"
    assert Path.new("//b/cc//") + "/a" == "/b/cc/a"
    assert Path.new("a/b/c") + "////d/./dd/..////" == "a/b/c/d"
    assert Path.new(".") + "/" == "."
    assert Path.new(".") + "." == "."
    assert Path.new(".") + "abc" == "abc"
    assert Path.new("..") + "a/../b" == "../b"
    assert Path.new("a/b/c") + "../AAA/../../b/c/d"

    # Test for panics after breaking out of the original path.

    begin Path.new(".") + ".."
    rescue Path::InvalidError; else assert false; end

    begin Path.new(".") + "/./asdf/../a/../../.."
    rescue Path::InvalidError; else assert false; end

    assert Path.new("/") + ".." == "/"

    begin Path.new("..") + ".."
    rescue Path::InvalidError; else assert false; end

    begin Path.new("../../..") + "a/../.././b"
    rescue Path::InvalidError; else assert false; end

    assert Path.new("..") + "..a" == "../..a"
    assert Path.new("a..") + "../a.." == "a.."

    begin Path.new("a/b") + ".."
    rescue Path::InvalidError; else assert false; end

    begin Path.new("../a") + "../b"
    rescue Path::InvalidError; else assert false; end

    # ascend!

    _ = Path.new("a")
    assert _.descend!("b") == "a/b" && _ == "a/b"
  end

  def test_join
    assert Path.new("a").join("b") == "a/b"
    assert Path.new("/").join("a") == "/a"
    assert Path.new("//b/cc//").join("/a") == "/a"
    assert Path.new("a/b/c").join("////d/./dd/..////") == "/d"
    assert Path.new(".").join("/") == "/"
    assert Path.new(".").join(".") == "."
    assert Path.new(".").join("abc") == "abc"
    assert Path.new("..").join("a/../b") == "../b"
    assert Path.new("a").join("..") ==  "."
    assert Path.new(".").join("..") ==  ".."
    assert Path.new("/aa/b/cc/d//e/").join("../e/../../../c") ==  "/aa/b/c"

    # join!

    _ = Path.new("a")
    assert _.join!("b") == "a/b" && _ == "a/b"
  end

  def test_has_ext
    assert Path.new("a.txt").has_ext? ".txt"
    assert not(Path.new("a.txt").has_ext? "a.txt")
    assert Path.new("/a/b.txt").has_ext? ".txt"
    assert not(Path.new("/aa/b.txt").has_ext? "b.txt")
    assert not(Path.new("/aa/b.txt").has_ext? "/b.txt")
    assert not(Path.new("/aa/b.txt").has_ext? "a/b.txt")
    assert Path.new("abc").has_ext? "bc"
    assert not(Path.new("abc").has_ext? "abc")
  end

  def test_add_ext
    assert Path.new("a").add_ext("b") == "ab"
    assert Path.new("a/b").add_ext(".txt") == "a/b.txt"

    # Test invalid extensions.

    begin Path.new("a").add_ext("b/c")
    rescue ArgumentError; else assert false; end

    # Test invalid cases of suffixing "." or "/".

    begin Path.new(".").add_ext("a")
    rescue Path::InvalidError; else assert false; end

    begin Path.new("/").add_ext("a")
    rescue Path::InvalidError; else assert false; end

    # add_ext!

    _ = Path.new("a")
    assert _.add_ext!(".txt") == "a.txt" && _ == "a.txt"
  end

  def test_rm_ext
    assert Path.new("a.txt").rm_ext(".txt") == "a"
    assert Path.new("a.txt").rm_ext("a.txt") == "a.txt"
    assert Path.new("/a/b.txt").rm_ext(".txt") == "/a/b"
    assert Path.new("/aa/b.txt").rm_ext("b.txt") == "/aa/b.txt"
    assert Path.new("/aa/b.txt").rm_ext("/b.txt") == "/aa/b.txt"
    assert Path.new("/aa/b.txt").rm_ext("a/b.txt") == "/aa/b.txt"
    assert Path.new("abc").rm_ext("bc") == "a"
    assert Path.new("abc").rm_ext("abc") == "abc"

    # rm_ext!

    _ = Path.new("a.txt")
    assert _.rm_ext!(".txt") == true && _ == "a"
    assert _.rm_ext!(".xml") == false && _ == "a"
  end
end
