require "debug"

require "snx2"
require "test/unit"

include Snx2

class EnvTest < Test::Unit::TestCase
  def env_eq(env, **args)
    return false if env.dests != (args[:dests] || [])
    return false if env.emitters.hash != (args[:emitters] || {})
    return false if env.links.generic_paths != (args[:links_generic_paths] || {})
    return false if env.links.paths != (args[:links_paths] || {})
    return false if env.links.prefixes != (args[:links_prefixes] || {})
    return false if env.outputs.hash != (args[:outputs] || {})
    return false if env.vars.hash != (args[:vars] || {})
    true
  end

  def test_propagation
    assert(Env.global == nil)

    (olist = OutputList.new)[:html] = Output.new

    use = Env.new do |e|
      e.dests.push :dest_one
      e.dests.push :dest_two
      e.emitters[:html] = "use/emitter_one"
      e.emitters[:gemini] = "use/emitter_two"
      e.links.generic_paths[%w{generic one}] = OutputList.new(olist, "use/generic_one")
      e.links.generic_paths[%w{generic two}] = OutputList.new(olist,"use/generic_two")
      e.links.paths["path/one"] = OutputList.new(olist, "use/path_one")
      e.links.paths["path/two"] = OutputList.new(olist, "use/path_two")
      e.links.prefixes["prefix_one"] = Path.new("use/prefix_one")
      e.links.prefixes["prefix_two"] = Path.new("use/prefix_two")
      e.outputs[:html] = Output.new(nil, "use/output_one")
      e.outputs[:gemini] = Output.new(nil, "use/output_two")
      e.vars[:one] = "use/var_one"
      e.vars[:two] = "use/var_two"
    end

    from = Env.new do |e|
      e.dests.push :dest_three
      e.emitters[:gemini] = "from/emitter_two"
      e.emitters[:gopher] = "from/emitter_three"
      e.links.generic_paths[%w{generic two}] = OutputList.new(olist,"from/generic_two")
      e.links.generic_paths[%w{generic three}] = OutputList.new(olist,"from/generic_three")
      e.links.paths["path/two"] = OutputList.new(olist, "from/path_two")
      e.links.paths["path/three"] = OutputList.new(olist, "from/path_three")
      e.links.prefixes["prefix_two"] = Path.new("from/prefix_two")
      e.links.prefixes["prefix_three"] = Path.new("from/prefix_three")
      e.outputs[:gemini] = Output.new(nil, "from/output_two")
      e.outputs[:gopher] = Output.new(nil, "from/output_three")
      e.vars[:two] = "from/var_two"
      e.vars[:three] = "from/var_three"
    end

    env = Env.new do |e|
      e.dests.push :dest_three
      e.dests.push :dest_four
      e.emitters[:gopher] = "env/emitter_three"
      e.emitters[:php] = "env/emitter_four"
      e.links.generic_paths[%w{generic three}] = OutputList.new(olist,"env/generic_three")
      e.links.generic_paths[%w{generic four}] = OutputList.new(olist,"env/generic_four")
      e.links.paths["path/three"] = OutputList.new(olist, "env/path_three")
      e.links.paths["path/four"] = OutputList.new(olist, "env/path_four")
      e.links.prefixes["prefix_three"] = Path.new("env/prefix_three")
      e.links.prefixes["prefix_four"] = Path.new("env/prefix_four")
      e.outputs[:gopher] = Output.new(nil, "env/output_three")
      e.outputs[:php] = Output.new(nil, "env/output_four")
      e.vars[:three] = "env/var_three"
      e.vars[:four] = "env/var_four"
    end

    specific = {
      dests: [ :dest_four, :dest_five ],
      emitters: { php: "specific/emitter_four", yak: "specific/emitter_five" },
      links: Links.new.tap do |links|
        links.generic_paths[%w{generic four}] = OutputList.new(olist,"specific/generic_four")
        links.generic_paths[%w{generic five}] = OutputList.new(olist,"specific/generic_five")
        links.paths["path/four"] = OutputList.new(olist, "specific/path_four")
        links.paths["path/five"] = OutputList.new(olist, "specific/path_five")
        links.prefixes["prefix_four"] = Path.new("specific/prefix_four")
        links.prefixes["prefix_five"] = Path.new("specific/prefix_five")
      end,
      outputs: {
        php: Output.new(nil, "specific/output_four"),
        yak: Output.new(nil, "specific/output_five"),
      },
      vars:  {
        four: "specific/var_four",
        five: "specific/var_five",
      },
    }

    expected = {
      dests: [ :dest_one, :dest_two, :dest_three, :dest_four, :dest_five ],
      emitters: {
        html: "use/emitter_one",
        gemini: "from/emitter_two",
        gopher: "env/emitter_three",
        php: "specific/emitter_four",
        yak: "specific/emitter_five",
      },
      links_generic_paths: {
        %w{generic one} => OutputList.new(olist, "use/generic_one"),
        %w{generic two} => OutputList.new(olist,"from/generic_two"),
        %w{generic three} => OutputList.new(olist,"env/generic_three"),
        %w{generic four} => OutputList.new(olist,"specific/generic_four"),
        %w{generic five} => OutputList.new(olist,"specific/generic_five"),
      },
      links_paths: {
        "path/one" => OutputList.new(olist, "use/path_one"),
        "path/two" => OutputList.new(olist, "from/path_two"),
        "path/three" => OutputList.new(olist, "env/path_three"),
        "path/four" => OutputList.new(olist, "specific/path_four"),
        "path/five" => OutputList.new(olist, "specific/path_five"),
      },
      links_prefixes: {
        "prefix_one" => Path.new("use/prefix_one"),
        "prefix_two" => Path.new("from/prefix_two"),
        "prefix_three" => Path.new("env/prefix_three"),
        "prefix_four" => Path.new("specific/prefix_four"),
        "prefix_five" => Path.new("specific/prefix_five"),
      },
      outputs: {
        html: Output.new(nil, "use/output_one"),
        gemini: Output.new(nil, "from/output_two"),
        gopher: Output.new(nil, "env/output_three"),
        php: Output.new(nil, "specific/output_four"),
        yak: Output.new(nil, "specific/output_five"),
      },
      vars: {
        one: "use/var_one",
        two: "from/var_two",
        three: "env/var_three",
        four: "specific/var_four",
        five: "specific/var_five",
      },
    }

    use.use { assert(env_eq(Env.new(from, env: env, **specific), **expected)) }
  end

  def test_use_method
    e1 = Env.new
    assert env_eq(e1)
    e1.use do |e|
      assert(e == e1)
      assert(Env.global == e)
      e.dests.push :html, :gemini
      e.emitters[:html] = :irrelevant

      assert env_eq(e, dests: [:html, :gemini], emitters: {html: :irrelevant})

      Env.new do |f|
        # Ensure this env inherits from the global one.
        assert env_eq(f, dests: [:html, :gemini], emitters: {html: :irrelevant})

        # Try modifying this internal env.
        f.dests.push :gopher
        f.emitters[:gemini] = :irrelevant
        assert env_eq(f, dests: [:html, :gemini, :gopher],
                      emitters: {html: :irrelevant, gemini: :irrelevant})

        Env.new do |g|
          assert env_eq(g, dests: [:html, :gemini, :gopher],
                        emitters: {html: :irrelevant, gemini: :irrelevant})
        end

        e.dests.push :bogus
        e.outputs[:html] = Path.new("pathy")

        # Ensure that changing the outer env didn't change the inner one.
        assert env_eq(f, dests: [:html, :gemini, :gopher],
                      emitters: {html: :irrelevant, gemini: :irrelevant})
      end

      # Ensure the external env wasn't modified by the internal one.
      assert env_eq(e, dests: [:html, :gemini, :bogus],
                    emitters: {html: :irrelevant},
                    outputs: {html: Path.new("pathy")})
    end
  end

  def test_sanitize
    e = Env.new

    e.dests.push :symbol
    e.dests.push "string"

    # The string should be translated to a symbol.
    assert env_eq(e, dests: [:symbol, :string])

    # Ensure invalid Env::Dests input is handled appropriately.

    begin e.dests.push 5318008
    rescue ArgumentError; else assert false; end

    begin Env.new(e, dests: 234)
    rescue ArgumentError; else assert false; end

    begin; Env.new(e, dests: [234])
    rescue ArgumentError; else assert false; end

    Env.new(e, dests: [:abc])
  end
end
